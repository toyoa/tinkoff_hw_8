package com.tinkoff.olegtojgildin.tinkoff_hw_8.entities;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Relation;

import java.util.List;
import java.util.Objects;

/**
 * Created by olegtojgildin on 20/11/2018.
 */

public class NodeWithRelations {
    @Embedded
    private Node node;

    @Relation(
            parentColumn = "id",
            entityColumn = "parentId",
            entity = NodeChildern.class,
            projection = "childId")
    private List<Long> childrenIds;

    @Relation(
            parentColumn = "id",
            entityColumn = "childId",
            entity = NodeChildern.class,
            projection = "parentId")
    private List<Long> parentIds;

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public List<Long> getChildrenIds() {
        return childrenIds;
    }

    public void setChildrenIds(List<Long> childrenIds) {
        this.childrenIds = childrenIds;
    }

    public List<Long> getParentIds() {
        return parentIds;
    }

    public void setParentIds(List<Long> parentIds) {
        this.parentIds = parentIds;
    }


}