package com.tinkoff.olegtojgildin.tinkoff_hw_8;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import com.tinkoff.olegtojgildin.tinkoff_hw_8.entities.Node;
import com.tinkoff.olegtojgildin.tinkoff_hw_8.entities.NodeChildern;
import com.tinkoff.olegtojgildin.tinkoff_hw_8.entities.NodeWithRelations;

import java.util.List;

/**
 * Created by olegtojgildin on 20/11/2018.
 */

@Dao
public interface NodeDao {

    @Transaction
    @Query("SELECT * FROM nodes")
    LiveData<List<NodeWithRelations>> getAllNodes();

    @Transaction
    @Query("SELECT * FROM nodes WHERE nodes.id!=:nodeId")
    LiveData<List<NodeWithRelations>> getAllNodesWithoutRelated(long nodeId);

    @Insert
    void addNode(Node node);

    @Insert
    void addChild(NodeChildern nodeChildren);

    @Delete
    void removeChild(NodeChildern nodeChildren);
}
