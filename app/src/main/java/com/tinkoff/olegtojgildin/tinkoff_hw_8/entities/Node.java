package com.tinkoff.olegtojgildin.tinkoff_hw_8.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by olegtojgildin on 20/11/2018.
 */

@Entity(tableName = "Nodes")
public class Node
{
    @PrimaryKey(autoGenerate = true)
    private long id;

    private int value;


    public Node(int id,int value)
    {
        this.id=id;
        this.value=value;
    }
    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
