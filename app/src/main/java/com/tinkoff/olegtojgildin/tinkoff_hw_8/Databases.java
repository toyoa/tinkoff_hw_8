package com.tinkoff.olegtojgildin.tinkoff_hw_8;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.tinkoff.olegtojgildin.tinkoff_hw_8.entities.Node;
import com.tinkoff.olegtojgildin.tinkoff_hw_8.entities.NodeChildern;

/**
 * Created by olegtojgildin on 20/11/2018.
 */

@Database(entities = {Node.class, NodeChildern.class}, version = 1)
public abstract class Databases extends RoomDatabase
{
    public static Databases getDatabase(final Context context) {
        return Room.databaseBuilder(context.getApplicationContext(), Databases.class,  "node_database").build();
    }

    public abstract NodeDao nodeDao();
}
